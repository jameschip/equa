#! /bin/bash


pdflatex beast.tex
pdflatex dunge.tex
pdflatex equa.tex
pdflatex core.tex

mkdir -p out

mv *.pdf out/

rm *.aux
rm *.log

